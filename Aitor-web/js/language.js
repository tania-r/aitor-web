function onClick()
{
  // obtenemos el texto y los idiomas origen y destino
  var dstLang = document.getElementById("dstLang").value;

  // llamada al traductor
  google.language.translate(dstLang, function(result) 
    {
      if (!result.error) 
      {    
        var resultado = document.getElementById("result");    
        resultado.innerHTML = result.translation;  
      }
      else alert(result.error.message);
    }
  );
}