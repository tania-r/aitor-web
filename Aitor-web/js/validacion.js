//Validacion formulario
function validateForm(){
    console.log("Hola estoy validando");
    var valid = true;
    var name = document.forms["formConsulta"]["name"].value;
    var phone = document.forms["formConsulta"]["phone"].value;
    var email = document.forms["formConsulta"]["email"].value;
    console.log("valores de los camos " + name + " / " + phone +" / " + email );

    if (name.trim() == ""){
        document.getElementById("nameError").innerHTML = "El nombre no puede estar vacío";  
        valid = false;     
    }
    if(phone.trim() == ""){
        document.getElementById("phoneError").innerHTML = "El teléfono no puede estar vacío";
        valid = false;
    }
    if(email.trim() == ""){
        document.getElementById("emailError").innerHTML = "El email no puede estar vacío";
        valid = false;
    }
    return valid;
}

//Mapa

function ubicacion(){
  var map = L.map('mapa').setView([43.2634, -2.9369], 16);
  L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="http://cloudmade.com">CloudMade</a>',
    maxZoom: 19
  }).addTo(map);
  L.marker([43.2634, -2.9369],{draggable: false})
    .addTo(map)
    .bindPopup('Gabinete GV42')
    .openPopup();
}
  
function closeServiceModal(opcion){

    if(opcion == 'closeServices1'){
        document.getElementById('services2').classList.remove('hide');
        document.getElementById('services3').classList.remove('hide');
        document.getElementById('services1').classList.remove('show');
    }else if(opcion =='closeServices2'){
        document.getElementById('services1').classList.remove('hide');
        document.getElementById('services3').classList.remove('hide');
        document.getElementById('services2').classList.remove('show');
    }else if(opcion =='closeServices3'){
        document.getElementById('services1').classList.remove('hide');
        document.getElementById('services2').classList.remove('hide');
        document.getElementById('services3').classList.remove('show');
    }   
}
